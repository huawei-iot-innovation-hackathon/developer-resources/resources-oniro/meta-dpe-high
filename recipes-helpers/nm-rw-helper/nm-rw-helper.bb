SUMMARY = "Networkmanager RW helper"
DESCRIPTION = "Create RW overlay for networkmanager"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${WORKDIR}/LICENSE;md5=7388a7144776640b5c29ecbfa8358b44"

SRC_URI += "\
            file://nm-rw-helper.service \
            file://LICENSE \
            "
SRCREV = "${AUTOREV}"

DEPENDS += "systemd"

inherit deploy systemd
SYSTEMD_SERVICE_${PN} = "nm-rw-helper.service"

do_install() {
    install -d 0744 "${D}/etc/systemd/system"
    install -m 0644 "${WORKDIR}/nm-rw-helper.service" "${D}/etc/systemd/system/nm-rw-helper.service"
}

FILES_${PN} += "/usr/share/licenses/nm-rw-helper \
                /usr/share/licenses/nm-rw-helper/LICENSE \
                /etc/systemd \
                /etc/systemd/system \
                /etc/systemd/system/nm-rw-helper.service \
               "
