#!/bin/sh

BIN="/usr/bin/smart-lamp-demo"
CONFIGS_FILE="smart-lamp.conf"
CONFIGS_DIR="/run/mount/appdata/SmartLamp"
DEFAULT_CONFIGS_DIR="/etc/smart-lamp-demo/"

mkdir -p $CONFIGS_DIR
cd $CONFIGS_DIR
[ ! -f "$CONFIGS_DIR/$CONFIGS_FILE" ] && cp "$DEFAULT_CONFIGS_DIR/$CONFIGS_FILE" "$CONFIGS_DIR/$CONFIGS_FILE"

$BIN -c "$CONFIGS_DIR/$CONFIGS_FILE"

