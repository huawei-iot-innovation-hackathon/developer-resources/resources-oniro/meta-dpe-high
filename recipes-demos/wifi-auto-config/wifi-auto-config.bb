SUMMARY = "Initial boot script to configure WiFi adapter"
DESCRIPTION = "Script to configure WiFi adapter only at first boot, started as a systemd service which removes itself once finished"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${WORKDIR}/LICENSE;md5=7388a7144776640b5c29ecbfa8358b44"

SRC_URI =  " \
    file://LICENSE \
    file://wifi-auto-config.service \
    file://wifi-auto-config.sh \
"

inherit allarch systemd

NATIVE_SYSTEMD_SUPPORT = "1"
SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE_${PN} = "wifi-auto-config.service"

do_compile () {
}

do_install () {
    # License file
    install -d 0744 "${D}/usr/share/licenses/wifi-auto-config"
    install -m 0644 "${WORKDIR}/LICENSE" "${D}/usr/share/licenses/wifi-auto-config/LICENSE"

    # Init service configs
    install -d "${D}${systemd_unitdir}/system/"
    install -m 0644 "${WORKDIR}/wifi-auto-config.service" "${D}${systemd_unitdir}/system/wifi-auto-config.service"
    
    # Init script
    install -d "${D}/${sbindir}"
    install -m 0755 "${WORKDIR}/wifi-auto-config.sh" "${D}/${sbindir}/wifi-auto-config.sh"
}

FILES_${PN} += " \
	/usr/share/licenses/wifi-auto-config/LICENSE \
	"

