#!/bin/sh

CONFIGS_FILE="wifi-auto-config.conf"
CONFIGS_DIR="/run/mount/appdata/WIFI"

SSID="ASUS_80"
PWD="dpe_team_123"

[ -f "$CONFIGS_DIR/$CONFIGS_FILE" ] && source $CONFIGS_DIR/$CONFIGS_FILE

RES=1
while [[ $RES -ne 0 ]]; do
  sleep 5
  nmcli d wifi connect "$SSID" password "$PWD"
  RES=$?
done


#job done, remove it from systemd services
#systemctl disable wifi-auto-config.service

