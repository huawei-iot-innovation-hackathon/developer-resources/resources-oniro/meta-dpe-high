#!/bin/sh

BIN="/usr/bin/mqtt-manager"
CONFIGS_FILE="mqtt-manager.conf"
CONFIGS_DIR="/run/mount/appdata/MQTT"
DEFAULT_CONFIGS_DIR="/etc/mqtt-manager/"

mkdir -p $CONFIGS_DIR
cd $CONFIGS_DIR
[ ! -f "$CONFIGS_DIR/$CONFIGS_FILE" ] && cp "$DEFAULT_CONFIGS_DIR/$CONFIGS_FILE" "$CONFIGS_DIR/$CONFIGS_FILE"

$BIN -c "$CONFIGS_DIR/$CONFIGS_FILE"

