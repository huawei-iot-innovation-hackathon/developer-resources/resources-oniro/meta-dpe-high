# meta-dpe-high
Recipes for Hackathon resources.<br/>
See building instructions from [documentation/GettingStarted.md](https://gitlab.com/huawei-iot-innovation-hackathon/developer-resources/sample-project/documentation/-/blob/main/GettingStarted.md)

## Dependencies
Recipes brings Hackathon resources on top of Oniro release.

## Recipes
Here the list of the recipes provided by this layer:

* mqtt-manager:<br/>
  Local bridge from DBus to MQTT and vice-versa.<br/>
  It read is configs from `/run/mount/appdata/MQTT/mqtt-manager.conf` file. The
  file `/etc/mqtt-manager/mqtt-manager.conf' will be used if no config file is
  set.
* smart-lamp-demo:<br/>
  Device application that read and set status of GPIO via DBus messages<br/>
  It read is configs from `/run/mount/appdata/SmartLamp/smart-lamp-demo.conf`
  file. The file `/etc/smart-lamp-demo/smart-lamp-demo.conf' will be used if
  no config file is set.
* wifi-auto-configs:<br/>
  init script to configure WiFi credentials<br/>
  It read is configs from `/run/mount/appdata/WiFi/wifi-auto-config.conf`
  file. The script's default value will be used if no config file is set.
* nm-rw-helper:<br/>
  init script to mount the /etc/NetworkManager dir as a rw dir

## Distros
Here the list of the distros provided by this layer:

* smart-lamp-demo: demo that contains the smart-lamp-demo device application and the mqtt-manager packages as IoT tools.
